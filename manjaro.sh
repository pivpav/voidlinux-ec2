#!/bin/bash

set -x
set -e

# Available architectures:
# i686
# x86_64
# x86_64-musl
ARCH=${ARCH:=x86_64}
ROOT=/mnt/manjaro
DEV=${DEV:-/dev/xvdf}
IMAGE=${IMAGE:=manjarolinux/base}

# Creating single MBR partition using the whole drive
# We are leaving 1M for MBR record.
echo "Partitioning drive $DEV"
sfdisk $DEV << EOF
1M,,L
EOF
sync

echo "Formatting partition ${DEV}1 as ext4"
ROOT_DEV=${DEV}1
mkfs.ext4 $ROOT_DEV

echo "Installing docker"
yum install -y docker
systemctl start docker

echo "Mounting partition $ROOT_DEV to $ROOT"
[ -d $ROOT ] || mkdir -p $ROOT
mount $ROOT_DEV $ROOT

echo "Bootstrapping Manjaro image to $ROOT"
docker run --name manjaro manjarolinux/base
docker cp manjaro:/ $ROOT/
docker stop manjaro

echo "Mounting dev/proc/sys partitions to $ROOT"
mount --bind /dev $ROOT/dev
mount --bind /proc $ROOT/proc
mount --bind /sys $ROOT/sys

echo "Copying DNS configuration from host system"
cp /etc/resolv.conf $ROOT/etc/

echo "Generating fstab"
uuid=$(ls -l /dev/disk/by-uuid/ | grep $(basename $ROOT_DEV) | awk '{print $9}')
echo "UUID=$uuid / ext4 defaults 0 1" >> $ROOT/etc/fstab

echo "Generating script"
cat >$ROOT/tmp/install.sh <<EOCHROOT
set -x
set -e

. /etc/profile

ARCH=${ARCH}
USER=ec2-user
DEV=${DEV}
EOCHROOT

cat >>$ROOT/tmp/install.sh <<'EOCHROOT'
# Preparing image
pacman-mirrors --geoip
pacman --noconfirm -Syu
pacman --noconfirm -S base linux-latest linux-latest-headers dhcpcd grub mkinitcpio sudo git dkms openssh cloud-init

# DKMS ENA module
git clone https://github.com/amzn/amzn-drivers.git /usr/src/amzn-drivers

# Setting up variables
ENA_VER=$(cd /usr/src/amzn-drivers/kernel/linux/ena; git tag -l | sort -V | tail -1 | sed s'/ena_linux_//'g)
KERNEL_VER=$(ls /lib/modules | head -1)

ln -s /usr/src/amzn-drivers/kernel/linux/ena /usr/src/ena-${ENA_VER}
ln -s /usr/src/amzn-drivers/kernel/linux/common/ena_com /usr/src/ena-${ENA_VER}/

# Creating dkms.conf
cat >/usr/src/ena-${ENA_VER}/dkms.conf <<EOF
PACKAGE_NAME="ena"
PACKAGE_VERSION="${ENA_VER}"
MAKE[0]="make ENA_COM_PATH=./ena_com"
CLEAN="make ENA_COM_PATH=./ena_com clean"
BUILT_MODULE_NAME="ena"
DEST_MODULE_LOCATION[0]="/kernel/drivers/misc"
AUTOINSTALL="yes"
EOF

echo "ena" > /etc/modules-load.d/ena.conf

# Adding module to DKMS
BUILD_KERNEL=${KERNEL_VER} dkms install -m ena/${ENA_VER} -k ${KERNEL_VER} --force-version-override

# Initial system configuration
locale-gen
echo "manjaro" > /etc/hostname

echo "Installing GRUB"
[ -d /boot/grub ] || mkdir -p /boot/grub
grub-mkconfig > /boot/grub/grub.cfg
grub-install ${DEV}

systemctl enable sshd
systemctl enable dhcpcd
systemctl enable systemd-timesyncd
systemctl enable cloud-init
systemctl enable cloud-config
systemctl enable cloud-final
systemctl enable cloud-init-local

sed s"/name: arch/name: ${USER}/"g -i /etc/cloud/cloud.cfg
EOCHROOT

echo "Running Install script into $ROOT"
chroot $ROOT /bin/bash /tmp/install.sh

echo "Removing install script"
rm -f $ROOT/tmp/install.sh

sync
