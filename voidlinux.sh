#!/bin/bash

set -x
set -e

# Available architectures:
# i686
# x86_64
# x86_64-musl
ARCH=${ARCH:-x86_64}
ROOT=/mnt/void
DEV=${DEV:-/dev/xvdf}
REPO=${REPO:-https://alpha.de.repo.voidlinux.org/current/}
ISO=${ISO:-https://alpha.de.repo.voidlinux.org/live/current}
BUILD_DATE=${BUILD_DATE:-20191109}
HOST=${HOST:-voidlinux}

# Creating single MBR partition using the whole drive
# We are leaving 1M for MBR record.
echo "Partitioning drive $DEV"
sfdisk $DEV << EOF
1M,,L
EOF
sync

echo "Formatting partition ${DEV}1 as ext4"
ROOT_DEV=${DEV}1
mkfs.ext4 $ROOT_DEV

echo "Downloading image void-${ARCH}-ROOTFS-${BUILD_DATE}"
curl ${ISO}/void-${ARCH}-ROOTFS-${BUILD_DATE}.tar.xz -o /tmp/void_base.tar.xz

echo "Mounting partition $ROOT_DEV to $ROOT"
[ -d $ROOT ] || mkdir -p $ROOT
mount $ROOT_DEV $ROOT

echo "Bootstrapping VoidLinux base image to $ROOT"
tar -xvf /tmp/void_base.tar.xz --directory $ROOT

echo "Mounting dev/proc/sys partitions to $ROOT"
mount --bind /dev $ROOT/dev
mount --bind /proc $ROOT/proc
mount --bind /sys $ROOT/sys

echo "Copying DNS configuration from host system"
cp /etc/resolv.conf $ROOT/etc/

echo "Setting up hostname"
printf $HOST > $ROOT/etc/hostname

echo "Generating fstab"
uuid=$(ls -l /dev/disk/by-uuid/ | grep $(basename $ROOT_DEV) | awk '{print $9}')
echo "UUID=$uuid / ext4 defaults 0 1" >> $ROOT/etc/fstab

echo "Generating script"
cat >$ROOT/tmp/install.sh <<EOCHROOT
set -x
set -e

. /etc/profile

ARCH=${ARCH}
USER=ec2-user
PASSWD=${PASSWD}
REPO=${REPO}
DEV=${DEV}
EOCHROOT

cat >>$ROOT/tmp/install.sh <<'EOCHROOT'
echo "Installing base system"
XBPS_ARCH=${ARCH} xbps-install -R $REPO -Sy linux base-system grub openssh curl dracut

echo "Updating system"
XBPS_ARCH=${ARCH} xbps-remove -y base-voidstrap
XBPS_ARCH=${ARCH} xbps-install -Syu

# DKMS ENA module
xbps-install -y git dkms
git clone https://github.com/amzn/amzn-drivers.git /usr/src/amzn-drivers

# Setting up variables
ENA_VER=$(cd /usr/src/amzn-drivers/kernel/linux/ena; git tag -l | sort -V | tail -1 | sed s'/ena_linux_//'g)
KERNEL_VER=$(ls /lib/modules | sort -V | tail -1)

ln -s /usr/src/amzn-drivers/kernel/linux/ena /usr/src/ena-${ENA_VER}
ln -s /usr/src/amzn-drivers/kernel/linux/common/ena_com /usr/src/ena-${ENA_VER}/

# Creating dkms.conf
cat >/usr/src/ena-${ENA_VER}/dkms.conf <<EOF
PACKAGE_NAME="ena"
PACKAGE_VERSION="${ENA_VER}"
MAKE[0]="make ENA_COM_PATH=./ena_com"
CLEAN="make ENA_COM_PATH=./ena_com clean"
BUILT_MODULE_NAME="ena"
DEST_MODULE_LOCATION[0]="/kernel/drivers/misc"
AUTOINSTALL="yes"
EOF

mkdir -p /etc/modules-load.d
echo "ena" > /etc/modules-load.d/ena.conf

# Adding module to DKMS
BUILD_KERNEL=${KERNEL_VER} dkms install -m ena/${ENA_VER} -k ${KERNEL_VER} --force-version-override

echo "Installing GRUB"
[ -d /boot/grub ] || mkdir -p /boot/grub
grub-mkconfig > /boot/grub/grub.cfg
grub-install ${DEV}

echo "Enabling DHCP and SSH services"
ln -s /etc/sv/dhcpcd /etc/runit/runsvdir/default/
ln -s /etc/sv/sshd /etc/runit/runsvdir/default/

echo "Adding 'wheel' group to sudoers"
echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/wheel

echo "Creating user ${USER}"
useradd -G wheel,users ${USER}
if [ -z "$PASSWD" ]
then
    echo "${USER}:${PASSWD}" | chpasswd -c SHA512
fi
chpst -u ${USER} mkdir -p /home/${USER}/.ssh
chmod 700 /home/${USER}/.ssh

echo "Configure logging"
XBPS_ARCH=${ARCH} xbps-install -Sy socklog-void
ln -s /etc/sv/nanoklogd /etc/runit/runsvdir/default/
ln -s /etc/sv/socklog-unix /etc/runit/runsvdir/default/
usermod -a -G socklog ${USER}

# Creating cloudinit-ssh script
mkdir -p /etc/sv/cloudinit-ssh
cat >/etc/sv/cloudinit-ssh/run <<EOF
#!/bin/sh
exec chpst -u ${USER} curl http://169.254.169.254/latest/meta-data/public-keys/0/openssh-key -o /home/${USER}/.ssh/authorized_keys
EOF
chmod +x /etc/sv/cloudinit-ssh/run
ln -s /etc/sv/cloudinit-ssh /etc/runit/runsvdir/default/

# Creating first-run script
SRV_PATH=/etc/sv/first-run
mkdir -p /etc/sv/first-run
cat >/etc/sv/first-run/run <<'EOF'
#!/bin/sh
unlink /etc/runit/runsvdir/default/first-run

echo "Setting up hostname"
HOSTNAME=$(curl http://169.254.169.254/latest/meta-data/local-hostname)
echo $HOSTNAME > /etc/hostname
hostname $HOSTNAME

echo "Retreiving User-Data"
curl -f http://169.254.169.254/latest/user-data -o /tmp/user-data.sh 
if [ $? -eq 0 ]
then
    exec sh /tmp/user-data.sh 2>&1 > /var/log/user-data.log
fi
EOF
chmod +x /etc/sv/first-run/run
ln -s /etc/sv/first-run /etc/runit/runsvdir/default/
EOCHROOT

echo "Running Install script into $ROOT"
chroot $ROOT /bin/bash /tmp/install.sh

echo "Removing install script"
rm -f $ROOT/tmp/install.sh