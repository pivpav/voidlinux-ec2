# AMI building tool for VoidLinux and Manjaro.

I decided to automate custom AMI creation for two of the distributives I use the most [Void Linux](https://voidlinux.org/) and [Manjaro Linux](https://manjaro.org/). Both of them desktop-grade rolling release distributives, hence not suitable for production and are hard to find in AWS Marketplace.

## Creating AMI

In order to create your own AMI, please use `create_ami.sh` script. By default it creates AMI for Void Linux, however you can specify `voidlinux` or `manjaro` as script parameter in order to build the image you need.

Script also downloads and unpack `packer` tool in order to build AMI. Please note that `packer` require AWS credentials specified as environment variables.

## Void Linux

Unfortunately Void Linux doesn't support anything cloud-specific like AWS ENA network interface driver or `cloud-init` tool in order to propagate new user, User Data, etc.

In order to make Void Linux work image creation script is doing following:

1. Build AWS ENA kernel module using [official repository](https://github.com/amzn/amzn-drivers.git). I also created DKMS script for that module in order to rebuild module after every kernel upgrade.

2. `cloudinit-ssh` runit service to download SSH public key from the EC2 Metadata Service and install it to the `ec2-user` user, so linux can be accessed via SSH.

3. `first-run` runit service which will run at first boot in order to download User Data as shell script and execute it.

Of course this isn't even close to `cloud-init` functionality, but my target was to implement most used functionality, which at least makes me started.

## Manjaro Linux

Unlike Void Linux, Manjaro does have `cloud-init` package in order to properly configure instance, however still require ENA module to be compiled using exactly the same DKMS approach.

Also Manjaro doesn't provide ROOTFS archive (or I'm suck at Googling) so instead of unpacking full 1.5Gb+ ISO image, I decided to use `manjarolinux/base` docker image as system base image which is only around 200Mb.

### Current Manjaro issue.

Currently Manjaro builds and runs, however (!!!) during the first run server stuck with welcome message and offer to press any key in order to configure your installation. I'm not quite sure (yet) what exactly is missing in Manjaro configuration, but stopping and then starting the instance again (not rebooting though) helps, and the instance works fine afterwards.

## Logging to the instance

For both AMIs the default SSH user is `ec2-user`. This user doesn't have password for security reasons, so you will have to assign SSH key to the instance and use it to authenticate.

## ENA kernel module

This module is required if you want to use the whole range of AWS instances types. During image preparation this driver clones from the [official GitHub repository](https://github.com/amzn/amzn-drivers.git) and compiles using DKMS tool.

Because this module doesn't supply as a package, potentially, anything can go wrong with it after routine kernel upgrade. There are also no tools provided in order to keep this driver updated, so you should manually update it using `git pull` command and DKMS.

## Different Architectures

Initially I had courage to make a build process supporting not only `x86_64` but also `armv7l` and `x86_64-musl` variant of Void Linux, however `packer` currently doesn't fully support ARM installations, and I'm still having some weird issues with compiling ENA kernel module using `musl`.

The project is not fast-moving due to limited amount of free time, so any contributions are welcome.