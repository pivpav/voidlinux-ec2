#!/bin/bash

# Installing Pakcer.IO
if [ ! -f ./packer ]
then
    echo "No packer exist, downloading"
    curl https://releases.hashicorp.com/packer/1.6.4/packer_1.6.4_linux_amd64.zip -o packer.zip
    unzip ./packer.zip
    rm -f ./packer.zip
fi

# Possible architectures
# i686
# x86_64
# x86_64-musl
ARCH=${ARCH:=x86_64}
AWS_REGION=${AMI_REGION:=ap-southeast-2}
AWS_VPC=${AWS_VPC:=vpc-fc37c298}
AWS_SUBNET=${AWS_SUBNET:=subnet-624a803b}
SOURCE_AMI=${SOURCE_AMI:=ami-0f96495a064477ffb}

case $1 in
    ""|"voidlinux")
    AMI_NAME="voidlinux"
    AMI_DESCRIPTION="Void Linux AMI $ARCH"
    DISTRIB="voidlinux"
    ;;
    "manjaro")
    AMI_NAME="manjaro"
    AMI_DESCRIPTION="Manjaro Linux AMI ($ARCH)"
    DISTRIB="manjaro"
    ;;
esac

echo "Building AMI for $DISTRIB ($ARCH)"
./packer build \
    -var "aws_region=${AWS_REGION}" \
    -var "aws_vpc=${AWS_VPC}" \
    -var "aws_subnet=${AWS_SUBNET}" \
    -var "source_ami=${SOURCE_AMI}" \
    -var "ami_name=${AMI_NAME}" \
    -var "ami_description=${AMI_DESCRIPTION}" \
    -var "arch=${ARCH}" \
    -var "distrib=${DISTRIB}" \
    ./packer.json